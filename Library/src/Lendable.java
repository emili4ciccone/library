import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public abstract class Lendable {

    protected boolean lent;
    protected Calendar lendingDate;
    protected Calendar expirationDate;
    protected SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public boolean isLent() {
        return lent;
    }

    public void setLent(boolean lent) {
        this.lent = lent;
    }

    public abstract void lend(Date currentDate);

    protected void lendItem(Date currentDate, int durationInDays) {
        if (lendingDate == null) {
            lendingDate = Calendar.getInstance();
        }
        if (expirationDate == null) {
            expirationDate = Calendar.getInstance();
        }

        if (lent) {
            System.out.println("The item you chose is already lent. The current lent will end on " + sdf.format(expirationDate.getTime()) + ".\n");
        } else {
            lendingDate.setTime(currentDate);
            expirationDate.setTime(currentDate);
            updateLent(durationInDays);
            this.lent = true;
            System.out.println("The item has been lent to you.\nThe lent will end on " + sdf.format(expirationDate.getTime()) + ".\n");
        }
    }

    public void updateLent(int durationInDays) {
        this.expirationDate.add(Calendar.DAY_OF_YEAR, durationInDays);
    }

    public void giveBack(Date currentDate) {
        if (currentDate.after(expirationDate.getTime())) {
            System.out.println("You have returned the item after the lent expiration date.\n");
        } else {
            System.out.println("You have returned the item in time. Thank you!\n");
        }
        this.lent = false;
    }

    public Calendar getExpirationDate() {
        return this.expirationDate;
    }

}
