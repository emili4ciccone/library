import java.util.Date;

public class LendableBook extends Lendable {
    private Book book;

    public LendableBook(Book book) {
        this.book = book;
    }

    @Override
    public void lend(Date currentDate) {
        System.out.println("Your request: book, " + this.book.getTitle());
        super.lendItem(currentDate, 30);
    }
}
