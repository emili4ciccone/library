import java.util.Date;

public class LendableMagazine extends Lendable {

    private Magazine magazine;

    public LendableMagazine(Magazine m) {
        magazine = m;
    }
    @Override
    public void lend(Date currentDate) {
        System.out.println("Your request: magazine, " + this.magazine.getName());
        super.lendItem(currentDate, 15);
    }
}
