public class Main {
    public static void main(String[] args) {

        Library myLibrary = new Library();

        LendableBook alice = new LendableBook(new Book("Alice in Wonderland"));
        LendableBook flies = new LendableBook(new Book("Lord of the Flies"));
        LendableMagazine donnaModerna = new LendableMagazine(new Magazine("Donna Moderna"));
        myLibrary.addItem(alice);
        myLibrary.addItem(donnaModerna);
        myLibrary.lend(alice);
        myLibrary.lend(alice);
        myLibrary.lend(donnaModerna);
        myLibrary.lend(flies);

    }
}